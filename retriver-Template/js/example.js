$(function(){

    var $el, leftPos, newWidth,
        $mainNav = $("#example-one"),
        $mainNav2 = $("#example-two");
    
    /*
        EXAMPLE ONE
    */
    $mainNav.append("<li id='magic-line'></li>");
    
    var $magicLine = $("#magic-line");
    
    $magicLine
        .width($(".nav-wrap ul").width()-3)
        .css("left", $(".nav-wrap li a").position().left)
        .data("origLeft", $magicLine.position().left)
        .data("origWidth", $magicLine.width());
        
    $("#example-one li").find("a").hover(function() {
        $el = $(this);
        leftPos = $el.position().left;
        newWidth = $el.parent().width();
        
        $magicLine.stop().animate({
            left: leftPos,
            width: newWidth
        });
    }, function() {
        $magicLine.stop().animate({
            left: $magicLine.data("origLeft"),
            width: $magicLine.data("origWidth")
        });    
    });
    
    
        

});