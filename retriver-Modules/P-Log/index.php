<?php
/*		WebRetriver log ver 1.0
 * 
 *  	Create by PlayLinsor 14.12.2015
 */

$filePath="retriver-Core/TextLog.php";    //куда пишем логи
$countLine=4999;        		 //строк в файле не более

/// Функция записи в лог
function LogMessage($Text,$Type=2,$Why="САЙТ") {
	
	$filePath = $GLOBALS['filePath'];
	$countLine = $GLOBALS['countLine'];
	SaveLog($filePath, $countLine, $Text, $Why, $Type);
	
}
// Показываем лог
function ShowLog($Page = 1,$Count = 20) {
	if (!is_int($Page)) $Page = 1;
	$filePath = $GLOBALS['filePath'];    
	include_once $CONFIG["Module-log"]."template.php";
}
/////////// Private

function getRealIpAddr() {
  if (!empty($_SERVER['HTTP_CLIENT_IP']))        // Определяем IP
  { $ip=$_SERVER['HTTP_CLIENT_IP']; }
  elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))    // Если IP идёт через прокси
  { $ip=$_SERVER['HTTP_X_FORWARDED_FOR']; }
  else { $ip=$_SERVER['REMOTE_ADDR']; }
  return $ip;
}


function SaveLog($file,$count,$text,$why,$type) {    
	$ip = getRealIpAddr();
	$date = date("H:i:s d.m.Y");
	//$home = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];    //какая страница сайта
	$lines = file($file);        
	if (!$lines) $lines[] = "<? die() ?>\n";
	
	while(count($lines) > $count) array_shift($lines);
	
	$lines[] = $date."[ ".$why." ] { ".$type." }".$text." (<i> ".$ip." </i>)"."\r\n";
	file_put_contents($file, $lines);
}




?>