<?
/*		WebRetriver TEMPLATE-INDEX ver 1.0
 * 
 *  	Create by PlayLinsor 14.12.2015
 */
	if (!$_SESSION["OnSite"]) die("ACCESS DENAIDED");
?>

<!doctype html>
<html lang="ru">
<head>
	<meta charset="utf-8" />
	<title><? print($CONFIG["name"]." ".$CONFIG["ver"])?></title>
	<link rel="stylesheet" href="<?=$CONFIG["Current-Template"]?>/styles.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?=$CONFIG["Current-Template"]?>/css/movingbox.css" type="text/css" media="screen">
	
	<script src="<?=$CONFIG["Current-Template"]?>/js/jquery-1.3.1.min.js" type="text/javascript" charset="utf-8"></script>
	<!-- 
	<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js?ver=1.3.2'></script>
  	-->
  	<script type='text/javascript' src='<?=$CONFIG["Current-Template"]?>/js/example.js'></script>
    
    <script type='text/javascript' src='<?=$CONFIG["Current-Template"]?>/js/add-function.js'></script>
    <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>

<body>
<div id="wrapper">
<header>
	<div id="logo">&nbsp;</div>
    <h1><a href="#"><?=$CONFIG["name"]?></a></h1>
</header>


<div class="nav-wrap">

<? if ($_SESSION["USER"]!=null){ ?>
		<ul class="group" id="example-one">
            <li><a href="/retrive-admin.php">Главная</a></li>
            <li><a href="/retrive-admin.php?LogOut=1">Выход</a></li>
        </ul>
<? } ?>
</div>

<section id="main">
<article id="content">
	<? 
	if ($_SESSION["USER"]!=null){
		require('pages/index.php');
	}else {
		require('pages/login.php');
	}
	 ?>
</article>

</section><!-- end of #content -->

<footer>
<section id="footer-area">
	<section id="footer-outer-block">
		<div id="copyrigth"><a href="salactor.ru"> Наумко Дмитрий &copy; 2015 Salactor.ru</a></div>
	</section><!-- end of footer-outer-block -->
</section><!-- end of footer-area -->
</footer>
	
</div>
</body>
</html>
