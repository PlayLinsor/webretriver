<div class="Log-lines">
<?php

  $bufer = file_get_contents($filePath);
  preg_match_all("#([\d]+) ([^\n]+)(\n|$)#U",$bufer, $out,PREG_PATTERN_ORDER);

  // Формируем промежуточный массив
  for($i = 0; $i < count($out[1]); $i++)
		$temp[] = trim($out[2][$i]);


  $total = count($temp);
  $number = (int)($total/$Count);
  if((float)($total/$Count) - $number != 0) $number++;

  $start = (($Page - 1)*$Count);
  $end = $Page*$Count + 1;
  
  if($end > $total) $end = $total;
  
// Отображение строки
  for($i = $start; $i < $end; $i++)
    LinePrintLog($temp[$i]);
	
// Постраничная навигация
PageNavigatin($Count,$number,$total);
    
////////////////////////////////////////////////////////////////
function PageNavigatin($count,$number,$total) { 
	print ('<div class="log-navigation">'); 
  for($i = 1; $i <= $number; $i++)
  {
    if($i != $number)
    {
      if($count == $i)
      {
        echo "[".(($i - 1)*$count + 1)."-".$i*$count."]&nbsp;";
      }
      else
      {
        echo "<a href=$_SERVER[PHP_SELF]?LogPage=".$i.">[".
             (($i - 1)*$count + 1)."-".$i*$count."]</a>&nbsp;";
      }
    }
    else
    {
      if($count == $i)
      {
        echo "[".(($i - 1)*$count + 1)."-".($total - 1)."]&nbsp;";
      }
      else
      {
        echo "<a href=$_SERVER[PHP_SELF]?LogPage=".$i.">[".
             (($i - 1)*$count + 1)."-".($total - 1)."]</a>&nbsp;";
      }
    }
  }
  print ('</div>'); 
}
function LinePrintLog($Line)
{
	$STR_POS = strpos($Line,'[');
	$STR_END = strpos($Line,']')+1;
	$STR_STP = strpos($Line,'{')+1;
	$STR_ETP = strpos($Line,'}');

	$Dat = substr($Line,0,$STR_POS);
	$Type= substr($Line,$STR_POS,$STR_END-$STR_POS);
	$impotant = substr($Line,$STR_STP,$STR_ETP-$STR_STP);
	switch($impotant)
	{
		case 0: $imcolor = "#FFFFFF";		// Simple Text
				break;
		case 1: $imcolor = "#6BB5FF";		// Info Blue
				break;
		case 2: $imcolor = "#FFFF6B";		// Warning Yellow 
				break;
		case 3: $imcolor = "#00B82E";		// Susses Green
				break;
		case 4: $imcolor = "#FF6B6B";		// Error Red
				break;
		default:
				$imcolor = "#FFFFFF";
				$STR_ETP=$STR_END;
	}

	$Value= substr($Line,$STR_ETP+1,strlen($Line)-$STR_ETP);
	echo "<div style='background:".$imcolor."'><em>$Dat</em><strong> $Type </strong> $Value </div>";
}
?>

</div>