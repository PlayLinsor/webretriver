<?
error_reporting(E_ALL ^ E_NOTICE);
$old_error_handler = set_error_handler("myErrorHandler");

$urlRequest = $_SERVER["REQUEST_URI"];
$AllURL = $CONFIG["Ret-URL"].$CONFIG["Ret-Sep"].$urlRequest;

if ($CONFIG["Ret-manual-index"]!=Null) 
    if (getExtension($urlRequest)=="/"){
        $AllURL .= $CONFIG["Ret-manual-index"];
        
    }

// post-data
$PostData = "";
$text = "";
if ($_POST!=Null){
    foreach ($_POST as $key => $value){
        $PostData .= $key."=".$value."&";
    }
}

try {

    if( $curl = curl_init() ) {
        curl_setopt($curl, CURLOPT_URL, $AllURL);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $PostData);
        curl_setopt($curl, CURLOPT_COOKIEJAR, "cookie.txt");
        curl_setopt($curl, CURLOPT_COOKIEFILE,"cookie.txt");
        
        $text = curl_exec($curl);
        curl_close($curl);
    } else LogMessage("Ошибка Curl",$Type=2,"WEB-RET-MODULE");
 
    } catch (Exception $error){
        LogMessage("Общая Ошибка",$Type=2,"WEB-RET-MODULE");
    }
    
    
if($text!=NULL) {    
    PrintContentType(getExtension($urlRequest));    
    echo $text;
    

}else echo "fail";


function getExtension($filename) {
    $arExt = explode(".", $filename);
    return end($arExt);
}

////////////////////////////////////////////

function PrintContentType($ext) {
	$head = "";
	
	$mime_types = array(
            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',

            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        $ext = strtolower($ext);
        
        if (array_key_exists($ext, $mime_types)) {
            $head =  $mime_types[$ext];
        }else {
            $head = 'text/html';
        }
        
        header("Content-Type: ".$head); 
    }


function myErrorHandler($errno, $errstr, $errfile, $errline)
{
    if (!(error_reporting() & $errno)) {
        // This error code is not included in error_reporting
        return;
    }

    switch ($errno) {
    case E_USER_ERROR:
        echo "<b>My ERROR</b> [$errno] $errstr<br />\n";
        echo "  Fatal error on line $errline in file $errfile";
        echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
        echo "Aborting...<br />\n";
        exit(1);
        break;

    case E_USER_WARNING:
        echo "<b>My WARNING</b> [$errno] $errstr<br />\n";
        break;

    case E_USER_NOTICE:
        echo "<b>My NOTICE</b> [$errno] $errstr<br />\n";
        break;

    default:
        echo "Unknown error type: [$errno] $errstr<br />\n";
        break;
    }

    /* Don't execute PHP internal error handler */
    return true;
}
?>